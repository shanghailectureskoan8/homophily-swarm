# README #

Simulation for homophily using a swarm approach. This was done as a group project for the ShangAI lectures in 2015. There is a lot of room for improvement, for example in the way connections are made.

### Requirements ###

The simulation was written for NetLogo (https://ccl.northwestern.edu/netlogo/).